#!/usr/bin/python3

from bs4 import BeautifulSoup
import requests
import os
from pathlib import Path

MAGNET_HREF_INDEX = 3
RESULTS_TABLE_IDENTIFICATION = {'id': 'searchResult'}
DOWNLOAD_DIR = str(Path.home()) + '/Downloads'

search_string = input('Enter search string...\n')

url = 'https://thepiratebay.org/search/%s/0/99/0' % search_string

soup = BeautifulSoup(requests.get(url).text, 'html.parser')

search_results = soup.find('table', RESULTS_TABLE_IDENTIFICATION)

if search_results:
    torrent_accepted = False
    table_rows = search_results.find_all('tr')[1:]
    row_counter = 0
    num_rows = len(table_rows)
    
    while (not torrent_accepted) and (row_counter < num_rows):
        row = table_rows[row_counter]
        torrent_title = row.find('a' , {'class': 'detLink'}).text
        magnet_link = row.find_all('a')[MAGNET_HREF_INDEX]['href']
        seeds, leeches = row.find_all('td')[-2:]
        seeds = seeds.text
        leeches = leeches.text

        accepted = input("Title: %s has %s seeds and %s leeches. Download? (Y/y to download. Anything else to check next result...)\n" % (torrent_title, seeds, leeches))

        if accepted.upper() == 'Y':
            torrent_accepted = True

        row_counter += 1

    if not torrent_accepted:
        print('No more good seeds. Sorry :(')
        exit()

    result = os.system('rtorrent -d %s "%s"' % (DOWNLOAD_DIR, magnet_link))

    if result != 0:
        print('Error starting torrent download. Check that you have rtorrent installed.')
        exit()
    
    else:
        print('Check your downloaded files at %s' % DOWNLOAD_DIR)

else:
    print("Cant find that! Sorry :(")